<?php

namespace App\Controller;

use App\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Job;
use App\Entity\Candidature;

class JobController extends AbstractController
{
    #[Route('/job', name: 'app_job')]
    public function index(): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $job = new Job();
        $job->setType("architecte");
        $job->setCompany("offshoreBox");
        $job->setDescription("Genie logiciel");
        $job->setExpiresAt(new \DateTimeImmutable());
        $job->setEmail("raiesabdallah@gmail.com");
        $image = new Image();
        $image->setUrl('https://cdn.pixabay.com/photo/2015/10/30/10/03/gold-1013618_960_720.jpg');
        $image->setAlt('Job de reves');
        $job->setImage($image);
        $entityManager->persist($job);
        $entityManager->flush();
        //ajout de candidat
        $candidature1 = new Candidature();
        $candidature1->setCandidat("mohamed");
        $candidature1->setContenu("formation J2EE");
        $candidature1->setDateC(new \DateTime());
        $candidature1->setJob($job);
        $candidature2 = new Candidature();
        $candidature2->setCandidat("rami");
        $candidature2->setContenu("formation Symphony");
        $candidature2->setDateC(new \DateTime());
        $candidature2->setJob($job);
        $entityManager->persist($job);
        $entityManager->persist($candidature1);
        $entityManager->persist($candidature2);

        $entityManager->flush();

        return $this->render('job/index.html.twig', [
            'id' => $job->getid(),
        ]);
    }
    /**
     * @Route("/job/{id}", name="job_show")
     */
    public function show($id)
    {
        $job = $this->getDoctrine()
            ->getRepository(Job::class)
            ->find($id);

        $em = $this->getDoctrine()->getManager();
        $listCandidatures = $em->getRepository(Candidature::class)->findBy(['Job' => $job]);

        if (!$job) {
            throw $this->createNotFoundException(
                'No job found for id ' . $id
            );
        }

        return $this->render('job/show.html.twig', [
            'listCandidatures' => $listCandidatures,
            'job' => $job
        ]);
    }

}